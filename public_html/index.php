<?php
require $_SERVER['DOCUMENT_ROOT'] . "/app/settings.php";
require $_SERVER['DOCUMENT_ROOT'] . "/app/database.php";
require $_SERVER['DOCUMENT_ROOT'] . "/app/bootstrap.php";

include $_SERVER['DOCUMENT_ROOT'] . "/app/router.php";
