<?php
// Grabs the URI and breaks it apart in case we have querystring stuff
$request_uri = explode('/', $_SERVER['REQUEST_URI'], 3);
require_once CONTROLLERS_PATH . '/i18n.php';
require_once CONTROLLERS_PATH . '/utils.php';
require_once CONTROLLERS_PATH . '/categories.php';
require_once CONTROLLERS_PATH . '/currencies.php';
require_once CONTROLLERS_PATH . '/menu.php';
require_once CONTROLLERS_PATH . '/users.php';

use Twig\Extra\CssInliner\CssInlinerExtension;

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/templates/');
$twig = new \Twig\Environment($loader, [
  'cache' => CACHE_PATH,
]);
$twig->addExtension(new CssInlinerExtension());

$lang_file = LOCALE_PATH . "/" . $_SESSION['lang'] . ".php";
if(file_exists($lang_file)) {
  require LOCALE_PATH . "/" . $_SESSION['lang'] . ".php";
} else {
  require LOCALE_PATH . "/" . $GLOBALS['settings']['basic']['default_lang'] . ".php";
}
$twig->addGlobal('session', $_SESSION);
$twig->addGlobal('l', $L);
$twig->addGlobal('lifespan', $GLOBALS['settings']['basic']['session_lifespan']);
$twig->addGlobal('site_domain', $GLOBALS['settings']['basic']['site_domain']);
$twig->addGlobal('langs', $GLOBALS['settings']['basic']['enabled_langs']);
$twig->addGlobal('public_path', PUBLIC_PATH);

if (empty($_SESSION['token'])) {
  $_SESSION['token'] = bin2hex(random_bytes(32));
}

$utils = new Utils();
if(isset($_SESSION['last_action'])) {
  if((time() - $_SESSION['last_action']) < ($GLOBALS['settings']['basic']['session_lifespan'] * 60)) {
    $utils->keepalive();
  } else {
    $utils->logout();
  }
} else {
  $_SESSION['last_action'] = time();
}

try {
  $cache = new PageCache\PageCache();
  $cache->config()
  ->setCachePath(CACHE_PATH)
  ->setEnableLog(true)
  ->setCacheExpirationInSeconds(86400);
  $cache->init();
} catch (\Exception $e) {
  // Log PageCache error or simply do nothing.
  // In case of a PageCache error, the page will load normally, without cache.
}
//The rest of your PHP code, everything below will be cached
function getMenu($id, $json = true) {
  $items = [];
  $itms = new Menu();
  $items = $itms->getAllByUid($id, $json);

  return $items;
}

$profile = null;

// Route it up!

$usrs = new Users();
if(isset($_SESSION['user_id'])) {
  $profile = $usrs->getById($_SESSION['user_id'])[0];
}

if (is_numeric($request_uri[1])) {
  $id = intval($request_uri[1]);
  $profile = $usrs->getById($id)[0];
  $_SESSION['lang'] = $profile['lang'];
  include LOCALE_PATH . "/" . $_SESSION['lang'] . ".php";
  $crnnc = new Currency();
  $currency = $crnnc->getById($profile['currency'])[0];
  $menu = getMenu($id, false);
  echo $twig->render('food_menu.html.twig', ['food_menu' => $menu, 'profile' => $profile, 'currency' => $currency]);
} else {
  switch ($request_uri[1]) {
    case '':
    echo $twig->render('index.html.twig', ['profile' => $profile]);
    break;

    case '/':
    echo $twig->render('index.html.twig', ['profile' => $profile]);
    break;

    case 'home':
    echo $twig->render('index.html.twig', ['profile' => $profile]);
    break;

    case 'menu-editor':
    if(isset($_SESSION['user_id'])) {
      $menu = getMenu($_SESSION['user_id']);
    } else {
      $menu = null;
    }
    echo $twig->render('editor.html.twig', ['menu' => json_encode($menu), 'profile' => $profile]);
    break;

    case 'translator':
    if(isset($_SESSION['user_id'])) {
      $i18n = new I18n();
      $menu = getMenu($_SESSION['user_id']);
      $languages = $i18n->getLanguageList();
      $activeLangs = $i18n->getActiveLanguages($_SESSION['user_id']);
      $data = [
        'menu' => json_encode($menu),
        'profile' => $profile,
        'languages' => $languages,
        'activelangs' => json_encode($activeLangs)
      ];
      echo $twig->render('translator.html.twig', $data);
    } else {
      header('HTTP/1.0 401 Unauthorized');
      echo $twig->render('401.html.twig');
    }
    break;

    case 'profile':
    if(isset($_SERVER['HTTP_X_CSRFTOKEN'])) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $res = $usrs->update(
          $_SESSION['user_id'],
          $data->lang,
          $data->currency,
          $data->rest_name,
          $data->rest_desc,
          $data->rest_footnote
        );
        echo json_encode($res);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      $curs = new Currency();
      $currencies = $curs->getAll();
      $my_profile = $usrs->getById($_SESSION['user_id']);
      echo $twig->render('profile.html.twig', ['profile' => $my_profile[0], 'currencies' => $currencies]);
    }
    break;

    case 'add-category':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $json = file_get_contents('php://input');
        $ncat = json_decode($json);
        $cats = new Category();
        $cats->addCategory($_SESSION['user_id'], $ncat->name, $cats->nextOrder($_SESSION['user_id']));
        $menu = getMenu($_SESSION['user_id']);
        echo json_encode($menu);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'delete-category':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $json = file_get_contents('php://input');
        $dcat = json_decode($json);
        $cats = new Category();
        $res = $cats->delete($_SESSION['user_id'], $dcat->id);
        $menu = getMenu($_SESSION['user_id']);
        echo json_encode($menu);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'update-categories':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $cats = new Category();
        $json = file_get_contents('php://input');
        $ctgrs = json_decode($json);
        $i = 0;
        foreach ($ctgrs as $ctg) {
          $cats->setOrder($ctg->cid, $i);
          $i += 1;
        }
        echo "[{\"Result\": \"OK\"}]";
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'add-item':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $itm  = new Menu();
        $json = file_get_contents('php://input');
        $item = json_decode($json);
        $result = $itm->addItem($_SESSION['user_id'], $item);
        $response = $itm->getItemsByCategory($_SESSION['user_id'], $item->cid);
        echo json_encode($response);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'edit-item':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $itm  = new Menu();
        $json = file_get_contents('php://input');
        $item = json_decode($json);
        $result = $itm->update($_SESSION['user_id'], $item);
        $response = $itm->getItemsByCategory($_SESSION['user_id'], $item->cid);
        echo json_encode($response);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'move-item':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $itm  = new Menu();
        $json = file_get_contents('php://input');
        $item = json_decode($json);
        if($item->dir == 'up') {
          $result = $itm->move_up($_SESSION['user_id'], $item->id, $item->prev);
        } else {
          $result = $itm->move_down($_SESSION['user_id'], $item->id, $item->next);
        }
        $response = $itm->getItemsByCategory($_SESSION['user_id'], $item->cid);
        echo json_encode($response);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'delete-item':
    if($_SESSION['user_id']) {
      if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
        $json = file_get_contents('php://input');
        $ditem = json_decode($json);
        $itm = new Menu();
        $res = $itm->delete($_SESSION['user_id'], $ditem->id);
        if($_SESSION['user_id']) {
          $menu = getMenu($_SESSION['user_id']);
        }
        echo json_encode($menu);
      } else {
        header('HTTP/1.0 401 Unauthorized');
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'lang':
    $lng = $request_uri[2];
    $_SESSION['lang'] = $lng;
    header("Location: ". $_SERVER['HTTP_REFERER']);

    case 'keepalive':
    if(hash_equals($_SERVER['HTTP_X_CSRFTOKEN'], $_SESSION['token'])) {
      $utils->keepalive();
      echo json_encode("OK");
    }
    break;

    case 'register':
    $curs = new Currency();
    $currencies = $curs->getAll();
    echo $twig->render('register.html.twig', ['currencies' => $currencies]);
    break;

    case 'register-check':

    $email = "";
    $password = "";
    $verification = 1; // str_shuffle(md5(microtime())) . md5(microtime());
    $preflang = "en";
    $errors = array();

    if(isset($_POST['usr_registration'])) {
      $email = $_POST['email'];
      $password = $_POST['password'];
      $re_password = $_POST['re_password'];
      $preflang = $_POST['preflang'];
      $currency = $_POST['currency'];

      $email_check = $usrs->checkemail($email);

      if(empty($email)) { array_push($errors, "Email address is requied."); }
      if(empty($password)) { array_push($errors, "Password is requied."); }
      if($password != $re_password) { array_push($errors, "The two passwords do not match."); }

      if(!empty($email_check)) {
        array_push($errors, "A user with this email address already exists.<br>Try <a class='link' href='/login'>log in</a>.");
      }

      if(count($errors) == 0) {
        $result = $usrs->register($email, $password, $verification, $preflang, $currency);
      }

      if(count($errors) == 0) {
        echo $twig->render('index.html.twig', ['success' => $L['reg_success']]);
      } else {
        echo $twig->render('register.html.twig', ['error' => $L['reg_error'], 'errors' => $errors]);
      }
    } else {
      header('HTTP/1.0 401 Unauthorized');
    }
    break;

    case 'login':
    if(isset($_POST['usr_login'])) {
      $user = $_POST['email'];
      $password = $_POST['password'];

      $result = $usrs->login($user, $password);

      $profile = $usrs->getById($_SESSION['user_id'])[0];

      if($result == 'OK') {
        if($profile['rest_name'] == "") {
          header("Location: /profile");
        } else {
          header("Location: /");
        }
      } else {
        echo $twig->render('login.html.twig', ['login_res' => $result ]);
      }
    } else {
      echo $twig->render('login.html.twig');
    }
    break;

    case 'logout':
    $utils->logout();
    header("Location: /");
    break;

    case 'activate':
    require VIEWS_PATH . '/activation.php';
    break;

    default:
    header('HTTP/1.0 404 Not Found');
    echo $twig->render('404.html.twig');
    break;
  }
}
