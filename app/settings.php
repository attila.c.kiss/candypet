<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
require __DIR__ . '/vendor/autoload.php';

$GLOBALS['settings'] = array(
  "basic" => array(
    "site_domain" => "http://restmen.candypet.shop/",
    "session_lifespan" => 30, //in minutes
    "default_lang" => "en",
    "enabled_langs" => ['hu', 'en'],
  ),
  "mail" => array(
    "smtp_host" => "smtp.hostinger.gr",
    "smtp_port" => 587,
    "smtp_user" => "info@candypet.shop",
    "smtp_password" => "TeonDigital1!",
    "sender_email" => "info@candypet.shop",
    "sender_name" => "Restaurant Menu",
  ),
);

defined("SUBDIR") or define("SUBDIR", '');

// defined("PUBLIC_PATH") or define("PUBLIC_PATH", '/public_html');
defined("PUBLIC_PATH") or define("PUBLIC_PATH", SUBDIR . '/public_html');

defined("CACHE_PATH")
or define("CACHE_PATH", realpath(dirname(__FILE__) . '/cache'));

defined("LOCALE_PATH")
or define("LOCALE_PATH", realpath(dirname(__FILE__) . '/locale'));

defined("VENDOR_PATH")
or define("VENDOR_PATH", realpath(dirname(__FILE__) . '/vendor'));

defined("MODELS_PATH")
or define("MODELS_PATH", realpath(dirname(__FILE__) . '/models'));

defined("VIEWS_PATH")
or define("VIEWS_PATH", realpath(dirname(__FILE__) . '/views'));

defined("CONTROLLERS_PATH")
or define("CONTROLLERS_PATH", realpath(dirname(__FILE__) . '/controllers'));

defined("TEMPLATES_PATH")
or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));
