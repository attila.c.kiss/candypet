<?php

$L = array();

//-- v1.0.0
$L["food_menu"] = "Menu";
$L["login"] = "Login";
$L["logout"] = "Logout";
$L["register"] = "Register";
$L["menu_url"] = "Menu url";
$L["update_your_profile"] = "For a public menu please update your profile.";
$L["reg_success"] = "You are now registered on the site. Please log in.";
$L["reg_error"] = "There was an error during the registration. Please try again.";
$L["my_profile"] = "Profile";
$L["profile_success"] = "Profile successfully updated.";
$L["profile_error"] = "Error during updating your profile.";
$L["email"] = "E-mail";
$L["rest_name"] = "Restaurant name";
$L["rest_desc"] = "Restaurant description";
$L["rest_footnote"] = "Menu footnote\n(tax, service fee, etc.)";
$L["save"] = "Save";
$L["password"] = "Password";
$L["repassword"] = "Re-type password";
$L["accept_tnc"] = "I’ve read and agree to the Terms & Conditions and Privacy & Policy.";
$L["preflang"] = "Main language";
$L["currency"] = "Currency";
$L["new_category"] = "New category";
$L["add"] = "Add";
$L["delete"] = "Delete";
$L["cancel"] = "Cancel";
$L["up"] = "Move up";
$L["down"] = "Move down";
$L["top"] = "Move to top";
$L["bottom"] = "Move to bottom";
$L["edit"] = "Edit";
$L["warning"] = "Warning.";
$L["del_category"] = 'Do you really want to delete this category with all the foods included? Warning: This action cannot be undone.';
$L["add_item"] = "Add item";
$L["to_category"] = " to";
$L["to_category_2"] = "category";
$L["title"] = "Title";
$L["description"] = "Description";
$L["prices"] = "Prices";
$L["price"] = "Price";
$L["optional"] = "optional";
$L["s_price"] = "Small";
$L["l_price"] = "Large";
$L["vegetarian"] = "Vegetarian";
$L["glutenfree"] = "Gluten free";
$L["spicy"] = "Spicy";
$L["delete_item"] = "Delete item";
$L["del_item"] = 'Do you really want to delete this item? Warning: This action cannot be undone.';

//---- v1.0.1
$L["edit_menu"] = "Edit menu";
$L["translator"] = "Translator";
$L["select_language"] = "Select language to add new";
$L["add_language"] = "Add language";
$L["original"] = "Original";
$L["translation"] = "Translation";
