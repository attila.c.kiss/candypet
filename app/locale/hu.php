<?php

$L = array();

//-- v1.0.0
$L["food_menu"] = "Étlap";
$L["login"] = "Bejelentkezés";
$L["logout"] = "Kijelentkezés";
$L["register"] = "Regisztráció";
$L["menu_url"] = "Étlap linkje";
$L["update_your_profile"] = "Publikáláshoz frissítse az adatlapját!";
$L["reg_success"] = "Sikeres regisztáció. Kérem, jelentkezzen be!";
$L["reg_error"] = "Hiba történt a regisztráció során. Kérem, próbálja újra!";
$L["my_profile"] = "Adatlap";
$L["profile_success"] = "Adatlap sikeresen frissítve.";
$L["profile_error"] = "Hiba történt az adatlap frissítése során.";
$L["email"] = "E-mail";
$L["rest_name"] = "Étterem neve";
$L["rest_desc"] = "Étterem leírása";
$L["rest_footnote"] = "Étlap lábjegyzet\n(ÁFA, szervízdíj, stb.)";
$L["save"] = "Mentés";
$L["password"] = "Jelszó";
$L["repassword"] = "Jelszó újra";
$L["accept_tnc"] = "Elolvastam és elfogadom a Felhasználási Feltételekben foglaltakat.";
$L["preflang"] = "Elsődleges nyelv";
$L["currency"] = "Pénznem";
$L["new_category"] = "Új kategória";
$L["add"] = "Hozzáad";
$L["delete"] = "Törlés";
$L["cancel"] = "Mégsem";
$L["up"] = "Feljebb";
$L["down"] = "Lejjebb";
$L["top"] = "Felülre";
$L["bottom"] = "Alulra";
$L["edit"] = "Szerkeszt";
$L["warning"] = "Figyelem!";
$L["del_category"] = "Valóban törli a kategóriát, a benne lévő tételekkel együtt? Figyelem! Ez a művelet nem visszavonható.";
$L["add_item"] = "Tétel hozzáadása";
$L["to_category"] = "";
$L["to_category_2"] = "kategóriához";
$L["title"] = "Név";
$L["description"] = "Leírás";
$L["prices"] = "Ár";
$L["price"] = "Ár";
$L["s_price"] = "Kicsi";
$L["l_price"] = "Nagy";
$L["optional"] = "opcionális";
$L["vegetarian"] = "Vegetáriánus";
$L["glutenfree"] = "Glutén mentes";
$L["spicy"] = "Csípős";
$L["delete_item"] = "Tétel törlése";
$L["del_item"] = "Valóban törli a tételt? Figyelem! Ez a művelet nem visszavonható.";

//---- v1.0.1
$L["edit_menu"] = "Étlap szerkesztése";
$L["translator"] = "Fordítás";
$L["select_language"] = "Válasszony nyelvet";
$L["add_language"] = "Nyelv hozzáadása";
$L["original"] = "Eredeti";
$L["translation"] = "Fordítás";
