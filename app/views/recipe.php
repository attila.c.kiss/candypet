<?php
if(isset($_SESSION['recipe_id'])) {
  $recipe_id = $_SESSION['recipe_id'];
  unset($_SESSION['recipe_id']);
  require_once CONTROLLERS_PATH . "/recipes.php";
  require_once CONTROLLERS_PATH . "/quantities.php";
  require_once CONTROLLERS_PATH . "/units.php";
  require_once CONTROLLERS_PATH . "/utils.php";
  $recipes = new Recipes();
  $quantities = new Quantities();
  $units = new Units();
  $utils = new Utils();

  $rcp = $recipes->getById($recipe_id);
  $unts = $units->getAll();
  $quants = $quantities->getByRecipe($recipe_id);

  $kcal = 0;
  foreach($quants as $q) {
    $kcal += $q['ingredient']['kcal'];
  }

  require_once TEMPLATES_PATH . '/header.php';
  require_once TEMPLATES_PATH . '/menu.php';
  ?>

  <div class="page-container align-center justify-center recipe-page">
    <div class="content justify-center">
      <div class="container column justify-start align-center w100">
        <div class="header-pic-container">
          <div class="header-pic" style="background-image: url(<?=PUBLIC_PATH?>/images/recipes/<?=$rcp['pic'];?>);"></div>
        </div>
        <div class="recipe-header">
          <div class="lead-pic-container">
            <div class="lead-pic" style="background-image: url(<?=PUBLIC_PATH?>/images/recipes/<?=$rcp['pic'];?>);"></div>
          </div>
          <h1 class="text-l"><?=$rcp['title']?> <small>(<?=$kcal?> kcal)</small></h1>
        </div>
        <div class="intro-container  content-width">
          <p class="text-l"><?=$rcp['intro']?></p>
        </div>
        <div class="container row">
        </div>
        <div class="container recipe-content align-start content-width">
          <div class="ingredients-container">
            <h3>Ingredients</h3>
            <table>
              <?php foreach($quants as $quant) {
                echo "<tr title='" . $quant['ingredient']['kcal'] . " kcal'>";
                if($quant['quantity']['unit_symbol'] === 'totaste') {
                  echo "<td class='text-r'>&nbsp;</td><td>&nbsp;</td>";
                  echo "<td><strong>" . $quant['ingredient']['name'] . "</strong> <i>to taste</i></td>";
                } else {
                  echo "<td class='text-r'>" . $quant['quantity']['quant'] . "</td><td>" . $quant['quantity']['unit_symbol'] . "&nbsp;</td>";
                  echo "<td><strong>" . $quant['ingredient']['name'] . "</strong></td>";
                }
                echo "</tr>";
              } ?>
            </table>
          </div>
          <div class="instructions-container">
            <h3>Instructions</h3>
            <p><?=$rcp['instructions'];?></p>
          </div>
          <div class="notes-container">
            <h3>Notes</h3>
            <p><?=$rcp['notes'];?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  require_once TEMPLATES_PATH . '/scripts.php';
  require_once TEMPLATES_PATH . '/footer.php';
} else {
  header("Location: /");
}
