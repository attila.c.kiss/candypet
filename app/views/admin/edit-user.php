<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once CONTROLLERS_PATH . "/users.php";
    require_once CONTROLLERS_PATH . "/statuses.php";
    require_once CONTROLLERS_PATH . "/user-types.php";

    $users = new Users();
    $statuses = new Statuses();
    $user_types = new UserTypes();
    if(isset($_SESSION['edit_user'])) {
      $uid = intval($_SESSION['edit_user']);
      if($uid == 0) {
        $usr['id'] = 'new';
        $usr['email'] = '';
        $usr['username'] = '';
        $usr['user_type'] = '';
        $usr['status'] = 'active';
      } else {
        $usr = $users->getById($uid)[0];
      }
    } else {
      header("Location: /admin/users");
    }
    ?>

    <div class="page-container align-center justify-center admin-user admin">
      <div class="content column">
        <h1 class="text-c page-title">Edit User</h1>
        <div class="form">
          <form method="POST" action="/admin/users/update">
            <ul>
              <li>
                <label>ID:</label><input type="text" readonly name="uid" id="uid" value="<?=$usr['id']?>"></label>
              </li>
              <ul>
                <li>
                  <label for="email">Email:</label><input type="email" name="email" id="email" value="<?=$usr['email']?>">
                </li>
                <li>
                  <label for="uname">Username:</label><input type="text" name="uname" id="uname" value="<?=$usr['username']?>">
                </li>
                <?php if($usr['id'] == 'new') { ?>
                  <li>
                    <label for="password">Password:</label><input type="password" name="password" id="password" required>
                  </li>
                  <li>
                    <label for="re-password">Re-type password:</label><input type="password" name="re_password" id="re_password" required>
                  </li>
                <?php } ?>
                <li>
                  <label for="user_type">User type:</label>
                  <select id="user_type" name="user_type">
                    <?php
                    foreach ($user_types->getAll() as $i) {
                      echo "<option value='$i[name]'";
                      if($usr['user_type'] == $i['name']) {
                        echo " selected";
                      }
                      echo ">" . $i['name'] . "</option>";
                    }
                    ?>
                  </select>
                </li>
                <li>
                  <label for="status">Status:</label>
                  <select id="status" name="status">
                    <?php
                    foreach ($statuses->getAll() as $i) {
                      echo "<option value='$i[name]'";
                      if($usr['status'] == $i['name']) {
                        echo " selected";
                      }
                      echo ">" . $i['name'] . "</option>";
                    }
                    ?>
                  </select>
                </li>
                <li>
                  <a class="btn" href="/admin/users">Cancel</a>
                  <button class="btn" type="submit" name="<?php if($usr['id'] == 'new') { echo 'new_usr'; } else { echo 'update_usr'; } ?>">Save</button>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
      <?php
    } else {
      header("Location: /");
    }
  } else {
    header("Location: /");
  } ?>
