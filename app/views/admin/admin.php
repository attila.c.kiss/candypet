<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once TEMPLATES_PATH . '/header.php';
    require_once TEMPLATES_PATH . '/menu.php';
    ?>

    <div class="page-container align-center justify-center admin-home admin">
      <div class="content container row align-center justify-center">
        <a href="/admin/users"><div class="icon">👥</div>Users</a>
        <a href="/admin/ingredients"><div class="icon">🥚</div>Ingredients</a>
        <a href="/admin/recipes"><div class="icon">🍲</div>Recipes</a>
        <a href="/admin/meals"><div class="icon">🍛</div>Meals</a>
      </div>
    </div>
    <?php
    require_once TEMPLATES_PATH . '/scripts.php';
    require_once TEMPLATES_PATH . '/footer.php';
  } else {
    header("Location: /");
  }
} else {
  header("Location: /");
} ?>
