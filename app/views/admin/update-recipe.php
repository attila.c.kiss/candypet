<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] === "admin") {
    require_once CONTROLLERS_PATH . "/recipes.php";
    require_once CONTROLLERS_PATH . "/quantities.php";
    require_once CONTROLLERS_PATH . "/ingredients.php";
    require_once CONTROLLERS_PATH . "/units.php";
    $rcps = new Recipes();
    $quants = new Quantities();
    $unts = new Units();
    $ingredients = new Ingredients();

    $units = $unts->getAll();

    if(isset($_POST['update_rcp'])) {
      if(isset($_SESSION['edit_rcp'])) {
        $ex_rcp = $rcps->getById($_SESSION['edit_rcp']);
        $ex_quants = $quants->getByRecipe($_SESSION['edit_rcp']);
        unset($_SESSION['edit_rcp']);
        $ing_list = $_POST['ings'];

        if(!empty($ex_quants)) {
          $ing_array = [];
          $ex = json_encode($ex_quants);

          if(strcmp($ing_list, $ex) == 0) {
            $ing_list = $ex;
          }
        }

        if($ex_rcp['title'] != $_POST['title']) {
          $title = $_POST['title'];
        } else {
          $title = $ex_rcp['title'];
        }

        if($ex_rcp['intro'] != $_POST['intro']) {
          $intro = $_POST['intro'];
        } else {
          $intro = $ex_rcp['intro'];
        }

        if($ex_rcp['instructions'] != $_POST['instructions']) {
          $instructions = $_POST['instructions'];
        } else {
          $instructions = $ex_rcp['instructions'];
        }

        if($ex_rcp['notes'] != $_POST['notes']) {
          $notes = $_POST['notes'];
        } else {
          $notes = $ex_rcp['notes'];
        }

        if($ex_rcp['pic'] != $_POST['pic']) {
          $pic = $_POST['pic'];
        } else {
          $pic = $ex_rcp['pic'];
        }

        if($ex_rcp['preparing_time'] != $_POST['preparing_time']) {
          $preparing_time = $_POST['preparing_time'];
        } else {
          $preparing_time = $ex_rcp['preparing_time'];
        }

        if($ex_rcp['cooking_time'] != $_POST['cooking_time']) {
          $cooking_time = $_POST['cooking_time'];
        } else {
          $cooking_time = $ex_rcp['cooking_time'];
        }

        if($ex_rcp['status'] != $_POST['status']) {
          $status = $_POST['status'];
        } else {
          $status = $ex_rcp['status'];
        }
        $quants->update($ex_rcp['id'], $ing_list);
        $rcps->update($ex_rcp['id'], $title, $intro, $instructions, $notes, $preparing_time, $cooking_time, $status);
        header("Location: /admin/recipes");
      }
    } elseif(isset($_POST['new_rcp'])) {
      $last_id = $rcps->new($_POST['title'], $_POST['intro'], $_POST['instructions'], $_POST['notes'], $_POST['preparing_time'], $_POST['cooking_time'], $_POST['status']);
      $quants->new($last_id, $_POST['ings']);
      header("Location: /admin/recipes");
    } else {
      header('HTTP/1.0 403 Forbidden');
      echo "<div class=\"message\">403 Forbidden. :(</div>";
    }
  }
} else {
  header('HTTP/1.0 403 Forbidden');
  echo "<div class=\"message\">403 Forbidden. :(</div>";
}
