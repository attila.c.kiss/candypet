<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    if(isset($_GET['id'])) {
      $id = intval($_GET['id']);

      require_once CONTROLLERS_PATH . "/users.php";
      $users = new Users();

      $users->restore($id);
      header("Location: /admin/users");
    }
  }
}
