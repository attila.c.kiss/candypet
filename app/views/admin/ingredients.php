<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once TEMPLATES_PATH . '/header.php';
    require_once TEMPLATES_PATH . '/menu.php';
    require_once CONTROLLERS_PATH . "/ingredients.php";
    require_once CONTROLLERS_PATH . "/utils.php";

    $ingredients = new Ingredients();
    $utils = new Utils();
    unset($_SESSION['edit_ing']);

    $utils->addJS(PUBLIC_PATH . "/node_modules/datatables.net/js/jquery.dataTables.js");
    $utils->addJS(PUBLIC_PATH . "/scripts/start_dtable.js");
    ?>

    <div class="page-container align-center justify-center admin-user admin">
      <div class="ingredients content justify-center align-center">
        <div class="btn-container">
          <a class="btn" href="/admin/ingredients/edit">New ingredient</a>
        </div>
        <table class="data-table responsive">
          <thead>
            <tr>
              <th>Name</th>
              <th class="text-c">Calories</th>
              <th class="text-c">Protein</th>
              <th class="text-c">Carbs</th>
              <th class="text-c">Fat</th>
              <th class="text-c">Sodium</th>
              <th class="text-c">Fiber</th>
              <th class="text-c">Tags</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($ingredients->getAll() as $i) {
              ?>
              <tr <?php if($i['status'] == 'deleted') { echo "class='deleted'"; } ?>>
                <td><?=$i['name']?></td>
                <td class="text-c"><?=$i['calories']?></td>
                <td class="text-c"><?php if($i['protein'] === NULL) { echo "N/A";} else { echo $i['protein'] . "g"; }?></td>
                <td class="text-c"><?php if($i['carbs'] === NULL) { echo "N/A";} else { echo $i['carbs'] . "g"; }?></td>
                <td class="text-c"><?php if($i['fat'] === NULL) { echo "N/A";} else { echo $i['fat'] . "g"; }?></td>
                <td class="text-c"><?php if($i['sodium'] === NULL) { echo "N/A";} else { echo $i['sodium'] . "g"; }?></td>
                <td class="text-c"><?php if($i['fiber'] === NULL) { echo "N/A";} else { echo $i['fiber'] . "g"; }?></td>
                <td class="text-c">
                  <?php
                  $count = 0;
                  $all_tags = $ingredients->getTagByItem($i['id']);
                  foreach($all_tags as $tag) {
                    if(sizeof($all_tags) > 1 && $count < sizeof($all_tags) - 1) {
                      echo " ,";
                    }
                    echo $tag['name'];
                    $count++;
                  }
                  ?>
                </td>
                <td class="text-c">
                  <a class="link" href="/admin/ingredients/edit/<?=$i['id']?>">Edit</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <?php
    require_once TEMPLATES_PATH . '/scripts.php';
    require_once TEMPLATES_PATH . '/footer.php';
  } else {
    header("Location: /");
  }
} else {
  header("Location: /");
} ?>
