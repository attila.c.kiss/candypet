<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    if(isset($_GET['id'])) {
      $id = intval($_GET['id']);

      require_once CONTROLLERS_PATH . "/recipes.php";
      $recipes = new Recipes();

      $recipes->delete($id);
      header("Location: /admin/recipes");
    }
  }
}
