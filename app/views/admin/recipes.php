<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once TEMPLATES_PATH . '/header.php';
    require_once TEMPLATES_PATH . '/menu.php';
    require_once CONTROLLERS_PATH . "/recipes.php";
    require_once CONTROLLERS_PATH . "/utils.php";

    $recipes = new Recipes();
    $utils = new Utils();
    unset($_SESSION['edit_rcp']);

    $utils->addJS(PUBLIC_PATH . "/node_modules/datatables.net/js/jquery.dataTables.js");
    $utils->addJS(PUBLIC_PATH . "/scripts/start_dtable.js");
    ?>

    <div class="page-container align-center justify-center admin-recipes admin">
      <div class="content align-center justify-center">
        <div class="btn-container">
          <a class="btn" href="/admin/recipes/edit">New recipe</a>
        </div>
        <table class="data-table responsive">
          <thead>
            <tr>
              <th class="text-c w100">&nbsp;</th>
              <th class="text-l">Title</th>
              <th class="text-c">Tags</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($recipes->getAll() as $i) {
              ?>
              <tr <?php if($i['status'] == 'deleted') { echo "class='deleted'"; } ?>>
                <td class="text-c w100"><?php if($i['pic'] != "") { echo "<div class='tbl-thumb' style='background-image: url(" . PUBLIC_PATH . "/images/recipes/" . $i['pic'] . ")' alt='" . $i['title'] . "'>"; } else { echo "&nbsp;"; }?></td>
                  <td><?=$i['title']?></td>
                  <td class="text-c">
                    <?php
                    $count = 0;
                    $all_tags = $recipes->getTagByItem($i['id']);
                    foreach($all_tags as $tag) {
                      if(sizeof($all_tags) > 1 && $count < sizeof($all_tags) - 1) {
                        echo " ,";
                      }
                      echo $tag['name'];
                      $count++;
                    }
                    ?>
                  </td>
                  <td class="text-c">
                    <a class="link" href="/admin/recipes/edit/<?=$i['id']?>">Edit</a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <?php
        require_once TEMPLATES_PATH . '/scripts.php';
        require_once TEMPLATES_PATH . '/footer.php';
      } else {
        header("Location: /");
      }
    } else {
      header("Location: /");
    } ?>
