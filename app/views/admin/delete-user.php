<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    if(isset($_SESSION['edit_user'])) {
      $id = intval($_SESSION['edit_user']);
      unset($_SESSION['edit_user']);

      require_once CONTROLLERS_PATH . "/users.php";
      $users = new Users();

      $users->delete($id);
      header("Location: /admin/users");
    }
  }
}
