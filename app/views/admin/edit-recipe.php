<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once CONTROLLERS_PATH . "/recipes.php";
    require_once CONTROLLERS_PATH . "/statuses.php";
    require_once CONTROLLERS_PATH . "/utils.php";
    require_once CONTROLLERS_PATH . "/ingredients.php";
    require_once CONTROLLERS_PATH . "/quantities.php";
    require_once CONTROLLERS_PATH . "/units.php";

    $utils = new Utils();
    $rcp = new Recipes();
    $ingredients = new Ingredients();
    $statuses = new Statuses();
    $quantities = new Quantities();
    $units = new Units();

    if(isset($_SESSION['edit_rcp'])) {
      $uid = intval($_SESSION['edit_rcp']);
      if($uid == 0) {
        $rc['id'] = 'new';
        $rc['title'] = '';
        $rc['intro'] = '';
        $rc['instructions'] = '';
        $rc['notes'] = '';
        $rc['pic'] = '';
        $rc['preparing_time'] = '';
        $rc['cooking_time'] = '';
        $rc['status'] = 0;
        $_SESSION['edit_rcp'] = 0;
      } else {
        $rc = $rcp->getById($uid);
      }
    } else {
      header("Location: /admin/recipes");
    }
    require_once TEMPLATES_PATH . '/header.php';
    require_once TEMPLATES_PATH . '/menu.php';

    $utils->addJS(PUBLIC_PATH . "/node_modules/suneditor/dist/suneditor.min.js");
    $utils->addJS(PUBLIC_PATH . "/scripts/start_wysiwyg.js");

    ?>
    <script>
    var ingredients = <?php echo json_encode($quantities->getByRecipe($uid)); ?>;
    </script>
    <div class="page-container justify-center edit-recipe">
      <div class="container column">
        <h1 class="text-c page-title">🍲 Edit Recipe</h1>
        <div class="container row justify-center align-start">
          <div class="form">
            <form method="POST" action="/admin/recipes/update/">
              <ul>
                <li>
                  <label>ID:</label><input type="text" readonly name="rid" id="rid" value="<?=$rc['id']?>">
                </li>
                <li>
                  <label for="title">Title:</label><input type="text" name="title" id="title" value="<?=$rc['title']?>">
                </li>
                <li>
                  <label for="intro">Picture:</label>
                  <div class="container column">
                    <div class="current-pic-filename">
                      <img src="<?=PUBLIC_PATH?>/images/recipes/<?=$rc['pic']?>">
                    </div>
                    <input type="file" name="pic" id="pic">
                  </div>
                </li>
                <li>
                  <label>Ingredients:</label>
                  <div class="ing-display"></div>
                  <input type="hidden" name="ings" id="ings" value='<?php echo json_encode($quantities->getByRecipe($uid)); ?>'>
                </li>
                <li>
                  <label for="intro">Introduction:</label><textarea name="intro" id="intro"><?=$rc['intro']?></textarea>
                </li>
                <li>
                  <label for="instructions">Instructions:</label><textarea name="instructions" id="instructions"><?=$rc['instructions']?></textarea>
                </li>
                <li>
                  <label for="notes">Notes:</label><textarea name="notes" id="notes"><?=$rc['notes']?></textarea>
                </li>
                <li>
                  <label for="preparing_time">Preparing time (min):</label><input type="number" name="preparing_time" id="preparing_time" value="<?=$rc['preparing_time']?>">
                </li>
                <li>
                  <label for="cooking_time">Cooking time (min):</label><input type="number" name="cooking_time" id="cooking_time" value="<?=$rc['cooking_time']?>">
                </li>
                <li>
                  <label for="status">Status:</label>
                  <select id="status" name="status">
                    <?php
                    foreach ($statuses->getAll() as $i) {
                      echo "<option value='$i[name]'";
                      if($rc['id'] == 'new') {
                        if($i['name'] == 'active') {
                          echo " selected";
                        }
                      }else {
                        if($rc['status'] == $i['name']) {
                          echo " selected";
                        }
                      }
                      echo ">" . $i['name'] . "</option>";
                    }
                    ?>
                  </select>
                </li>
                <li>
                  <a class="btn" href="/admin/recipes">Cancel</a>
                  <button class="btn" type="submit" name="<?php if($rc['id'] == 'new') { echo 'new_rcp'; } else { echo 'update_rcp'; } ?>">Save</button>
                </li>
              </ul>
            </form>
          </div>
          <div class="recipe-ingredients">
            <h2>Add ingredient</h2>
            <input type="hidden" readonly id="ing-id" name="ing-id" value="">
            <div>
              <div class="ing-name"><span class="placeholder">Select an item below</span></div>
            </div>
            <div class="container row justify-space-between">
              <div>
                <input type="text" id="ing-quantity" name="ing-quantity" placeholder="Quantity">
              </div>
              <select id="ing-unit" name="ing-unit">
                <option class="text-c" disabled selected value="">Choose unit</option>
                <?php
                $prevType = '';
                foreach ($units->getAll() as $u) {
                  if($u['symbol'] !== "totaste") {
                    if($u['type'] !== $prevType) { ?>
                      <option class="text-c" disabled value="">-- <?=$u['type'];?> --</option>
                    <?php } ?>
                    <option value="<?=$u['id'];?>"><?=$u['symbol'];?></option>
                    <?php
                    $prevType = $u['type'];
                  }
                } ?>
              </select>
              <div class="to-taste container row align-center justify-space-between">
                <input type="checkbox" name="totaste" id="totaste"><label for="totaste">To taste</label>
              </div>
            </div>
            <button class="add-ingredient btn">Add to recipe</button>
            <ul class="ing-list">
              <?php
              foreach ($ingredients->getAll() as $i) { ?>
                <li data-iid="<?=$i['id'];?>"><?=$i['name'];?></li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php
    require_once TEMPLATES_PATH . '/scripts.php';
    require_once TEMPLATES_PATH . '/footer.php';
  } else {
    header("Location: /");
  }
} else {
  header("Location: /");
} ?>
