<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once CONTROLLERS_PATH . "/users.php";
    $users = new Users();
    if(isset($_POST['update_usr'])) {
      if(isset($_SESSION['edit_user'])) {
        $ex_usr = $users->getById($_SESSION['edit_user'])[0];
        unset($_SESSION['edit_user']);

        if($ex_usr['email'] != $_POST['email']) {
          $email = $_POST['email'];
        } else {
          $email = $ex_usr['email'];
        }

        if($ex_usr['username'] != $_POST['uname']) {
          $username = $_POST['uname'];
        } else {
          $username = $ex_usr['username'];
        }

        if($ex_usr['user_type'] != $_POST['user_type']) {
          $user_type = $_POST['user_type'];
        } else {
          $user_type = $ex_usr['user_type'];
        }

        if($ex_usr['status'] != $_POST['status']) {
          $status = $_POST['status'];
        } else {
          $status = $ex_usr['status'];
        }

        $users->update($ex_usr['id'], $email, $username, $user_type, $status);
        header("Location: /admin/users");
      }
    } elseif(isset($_POST['new_usr'])) {
      $users->new($_POST['email'], $_POST['password'], $_POST['uname'], $_POST['user_type'], $_POST['status']);
      header("Location: /admin/users");
    } else {
      header('HTTP/1.0 403 Forbidden');
      echo "<div class=\"message\">403 Forbidden. :(</div>";
    }
  }
} else {
  header('HTTP/1.0 403 Forbidden');
  echo "<div class=\"message\">403 Forbidden. :(</div>";
}
