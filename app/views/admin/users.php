<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once CONTROLLERS_PATH . "/users.php";
    require_once CONTROLLERS_PATH . "/utils.php";

    $users = new Users();
    $utils = new Utils();
    unset($_SESSION['edit_user']);

    $utils->addJS("/node_modules/datatables.net/js/jquery.dataTables.js");
    $utils->addJS("/scripts/start_dtable.js");
    ?>

    <div class="page-container align-center justify-center admin-user admin">
      <div class="content">
        <div class="btn-container">
          <a class="btn" href="/admin/users/edit">New user</a>
        </div>
        <table class="data-table responsive">
          <thead>
            <tr>
              <th class="text-r">ID</th>
              <th class="text-l">Email</th>
              <th class="text-l">Username</th>
              <th class="text-c">User type</th>
              <th class="text-c">Status</th>
              <th class="text-c">Last login</th>
              <th class="text-c">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($users->getAll() as $i) { ?>
              <tr <?php if($i['status'] == 'deleted') { echo "class='deleted'"; } ?>>
                <td class="text-r"><?=$i['id']?></td>
                <td class="text-l"><?=$i['email']?></td>
                <td class="text-l"><?=$i['username']?></td>
                <td class="text-c"><?=$i['user_type']?></td>
                <td class="text-c"><?=$i['status']?></td>
                <td class="text-c"><?=$i['last_login']?></td>
                <td class="text-c"><a class="link" href="/admin/users/edit/<?=$i['id']?>">Edit</a></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php } else {
    header("Location: /");
  }
} else {
  header("Location: /");
} ?>
