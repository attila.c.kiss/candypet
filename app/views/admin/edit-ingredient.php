<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once CONTROLLERS_PATH . "/ingredients.php";
    require_once CONTROLLERS_PATH . "/statuses.php";

    $ingr = new Ingredients();
    $statuses = new Statuses();

    if(isset($_SESSION['edit_ing'])) {
      $uid = intval($_SESSION['edit_ing']);
      if($uid == 0) {
        $in['id'] = 'new';
        $in['name'] = '';
        $in['calories'] = NULL;
        $in['protein'] = NULL;
        $in['carbs'] = NULL;
        $in['fat'] = NULL;
        $in['sodium'] = NULL;
        $in['fiber'] = NULL;
        $in['meat'] = false;
        $in['status'] = 0;
      } else {
        $in = $ingr->getById($uid);
      }
    } else {
      header("Location: /admin/ingredients");
    }
    require_once TEMPLATES_PATH . '/header.php';
    require_once TEMPLATES_PATH . '/menu.php';
    ?>
    <div class="page-container align-center justify-center admin-user admin">
      <div class="content column">
        <h1 class="text-c page-title">Edit Ingredient</h1>
        <div class="form">
          <form method="POST" action="/admin/ingredients/update">
            <ul>
              <li>
                <label>ID:</label><input type="text" readonly name="iid" id="iid" value="<?=$in['id']?>"></label>
              </li>
              <ul>
                <li>
                  <label for="email">Name:</label><input type="text" name="name" id="name" value="<?=$in['name']?>">
                </li>
                <li>
                  <label for="calories">Calories (kcal/100g):</label><input type="number" name="calories" id="calories" value="<?=$in['calories']?>">
                </li>
                <li>
                  <label for="">Protein (g/100g):</label><input type="text" name="protein" id="protein" value="<?=$in['protein']?>">
                </li>
                <li>
                  <label for="">Carbs (g/100g):</label><input type="text" name="carbs" id="carbs" value="<?=$in['carbs']?>">
                </li>
                <li>
                  <label for="">Fat (g/100g):</label><input type="text" name="fat" id="fat" value="<?=$in['fat']?>">
                </li>
                <li>
                  <label for="">Sodium (mg/100g):</label><input type="text" name="sodium" id="sodium" value="<?=$in['sodium']?>">
                </li>
                <li>
                  <label for="">Fiber (g/100g):</label><input type="text" name="fiber" id="fiber" value="<?=$in['fiber']?>">
                </li>
                <li>
                  <label for="status">Status:</label>
                  <select id="status" name="status">
                    <?php
                    foreach ($statuses->getAll() as $i) {
                      echo "<option value='$i[name]'";
                      if($in['id'] == 'new') {
                        if($i['name'] == 'active') {
                          echo " selected";
                        }
                      }else {
                        if($in['status'] == $i['name']) {
                          echo " selected";
                        }
                      }
                      echo ">" . $i['name'] . "</option>";
                    }
                    ?>
                  </select>
                </li>
                <li>
                  <a class="btn" href="/admin/ingredients">Cancel</a>
                  <button class="btn" type="submit" name="<?php if($in['id'] == 'new') { echo 'new_ing'; } else { echo 'update_ing'; } ?>">Save</button>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
      <?php
      require_once TEMPLATES_PATH . '/scripts.php';
      require_once TEMPLATES_PATH . '/footer.php';
    } else {
      header("Location: /");
    }
  } else {
    header("Location: /");
  } ?>
