<?php
if(isset($_SESSION['user']) && isset($_SESSION['user_type'])) {
  if($_SESSION['user_type'] == "admin") {
    require_once CONTROLLERS_PATH . "/ingredients.php";

    $ings = new Ingredients();

    if(isset($_POST['update_ing'])) {
      if(isset($_SESSION['edit_ing'])) {
        $ex_ing = $ings->getById($_SESSION['edit_ing']);
        unset($_SESSION['edit_ing']);

        if($ex_ing['name'] != $_POST['name']) {
          $name = $_POST['name'];
        } else {
          $name = $ex_ing['name'];
        }

        if($ex_ing['calories'] != $_POST['calories']) {
          $calories = $_POST['calories'];
        } else {
          $calories = $ex_ing['calories'];
        }

        if($ex_ing['protein'] != $_POST['protein']) {
          $protein = $_POST['protein'];
        } else {
          $protein = $ex_ing['protein'];
        }

        if($ex_ing['carbs'] != $_POST['carbs']) {
          $carbs = $_POST['carbs'];
        } else {
          $carbs = $ex_ing['carbs'];
        }

        if($ex_ing['fat'] != $_POST['fat']) {
          $fat = $_POST['fat'];
        } else {
          $fat = $ex_ing['fat'];
        }

        if($ex_ing['fiber'] != $_POST['fiber']) {
          $fiber = $_POST['fiber'];
        } else {
          $fiber = $ex_ing['fiber'];
        }

        if($ex_ing['sodium'] != $_POST['sodium']) {
          $sodium = $_POST['sodium'];
        } else {
          $sodium = $ex_ing['sodium'];
        }

        if($ex_ing['status'] != $_POST['status']) {
          $status = $_POST['status'];
        } else {
          $status = $ex_ing['status'];
        }

        $ings->update($ex_ing['id'], $name, $calories, $protein, $carbs, $fat, $fiber, $sodium, $status);
        header("Location: /admin/ingredients");
      }
    } elseif(isset($_POST['new_ing'])) {
      $last_id = $ings->new($_POST['name'], $_POST['calories'], $_POST['protein'], $_POST['carbs'], $_POST['fat'], $_POST['fiber'], $_POST['sodium'], $_POST['status']);
      header("Location: /admin/ingredients");
    } else {
      require VIEWS_PATH . '/403.php';
    }
  }
} else {
  require VIEWS_PATH . '/403.php';
}
