<?php
require_once TEMPLATES_PATH . '/header.php';
require_once TEMPLATES_PATH . '/menu.php';
?>

<div class="page-container align-center justify-center home">
  <div class="content">
    <div class="container align-center justify-center">
      <div class="recipe-list align-center justify-center">

      </div>
    </div>
  </div>
</div>
<?php
require_once TEMPLATES_PATH . '/scripts.php';
require_once TEMPLATES_PATH . '/footer.php';
