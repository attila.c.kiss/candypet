<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require_once CONTROLLERS_PATH . "/users.php";
$users = new Users();

$email = "";
$password = "";
$verification = 1; // str_shuffle(md5(microtime())) . md5(microtime());
$preflang = "en";
$errors = array();

if(isset($_POST['usr_registration'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $re_password = $_POST['re_password'];
  $preflang = $_POST['preflang'];

  $email_check = $users->checkemail($email);

  if(empty($email)) { array_push($errors, "Email address is requied."); }
  if(empty($password)) { array_push($errors, "Password is requied."); }
  if($password != $re_password) { array_push($errors, "The two passwords do not match."); }

  if(!empty($email_check)) {
    array_push($errors, "A user with this email address already exists.<br>Try <a class='link' href='/login'>log in</a>.");
  }

  if(count($errors) == 0) {
    $result = $users->register($email, $password, $verification, $preflang);
    if($result = "success") {
        /*
      $mail = new PHPMailer(true);
      try {
        $mail->IsSMTP();
        $mail->Host = $GLOBALS['settings']['mail']['smtp_host'];

        $mail->SMTPAuth = true;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $mail->Port       = $GLOBALS['settings']['mail']['smtp_port'];
        $mail->Username   = $GLOBALS['settings']['mail']['smtp_user'];
        $mail->Password   = $GLOBALS['settings']['mail']['smtp_password'];

        //Recipients
        $mail->setFrom($GLOBALS['settings']['mail']['sender_email'], $GLOBALS['settings']['mail']['sender_name']);
        $mail->addAddress($email);     // Add a recipient

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Please activate your Restaurant Menu account.';
        $mail->Body    = '<p>Your account has been registered. Please activate it
        by clicking the below button:</p><br><a style="display: block; width: 200px;
        line-height: 40px; font-weight: bold; text-decoration: none;
        background-color: #f0f; text-align: center; border-radius: 4px;
        color: #fff;" href="' . $GLOBALS['settings']['basic']['site_domain'] . '/activate?v=' . $verification . '">Activate</a>';

        $mail->send();
        echo '<div class="message">Registration successful. Activation email has been sent to the
        <strong>' . $email . '</strong> address. Please check your emails including spam/junk folder.</div>';
      } catch (Exception $e) {
        echo "<div class='reg-message'>Message could not be sent. Mailer Error: {$mail->ErrorInfo}</div>";
      }
        */
        header("Location: /");
    }
  } else {
    echo "<div class='message'>";
    echo "<div class='error'>";
    foreach ($errors as $error) :
      echo "<p>" . $error . "</p>";
    endforeach;
    echo "</div>";
    echo "</div>";
  }
}
