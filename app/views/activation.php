<?php
require_once CONTROLLERS_PATH . "/users.php";

$users = new Users();

if (isset($_GET['v'])) {
  $verification = $_GET['v'];
  $rslt = $users->activate($verification);

    echo "<div class='message'>";
  if($rslt) {
    echo "Account successfully activated.";
  } else {
    echo "Something went wrong: The account is already activated or a new activation email has requested.";
  }
  echo "</div>";
}
