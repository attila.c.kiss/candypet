<?php
require_once CONTROLLERS_PATH . "/users.php";
$users = new Users();

$email = "";
$password = "";
$verification = 1; // str_shuffle(md5(microtime())) . md5(microtime());
$preflang = "en";
$errors = array();

if(isset($_POST['usr_registration'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $re_password = $_POST['re_password'];
  $preflang = $_POST['preflang'];

  $email_check = $users->checkemail($email);

  if(empty($email)) { array_push($errors, "Email address is requied."); }
  if(empty($password)) { array_push($errors, "Password is requied."); }
  if($password != $re_password) { array_push($errors, "The two passwords do not match."); }

  if(!empty($email_check)) {
    array_push($errors, "A user with this email address already exists.<br>Try <a class='link' href='/login'>log in</a>.");
  }

  if(count($errors) == 0) {
    $result = $users->register($email, $password, $verification, $preflang);
    if($result != "success") {
      array_push($erros, $result);
      return $errors;
    } else {
      return "OK";
    }
  } else {
    return $errors;
  }
} else {
  header('HTTP/1.0 401 Unauthorized');
}