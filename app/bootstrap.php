<?php
session_start();

if(!isset($_SESSION['lang']) || strlen($_SESSION['lang']) != 2) {
  if(getenv("LANG")) {
    $elang = substr(getenv("LANG"), 0, 2);
    $_SESSION['lang'] = $elang;
  } else {
    $_SESSION['lang'] = 'en';
  }
}
