<?php
class I18n extends DB {
  function getLanguageList() {
    $list = $this->select('SELECT * FROM `i18n_languages` ORDER BY name;');
    return $list;
  }

  function getActiveLanguages($uid) {
    $aLangs = $this->select("SELECT lang FROM `categories_i18n` WHERE uid = " . $uid . " GROUP BY lang ORDER BY lang;");
    return $aLangs;
  }
}
