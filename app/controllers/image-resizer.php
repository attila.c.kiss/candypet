<?php
class ImageResizer {
  function createThumbnail($image_name, $thumbWidth, $thumbHeight) {
    $sourcePath = $uploadDir . '/' . $image_name;

    $sourcePathinfo = getimagesize($sourcePath);
    $imageDetail = pathinfo($sourcePath);

    $originalImage = imagecreatefromjpeg($sourcePath);

    $width          =   $imageDetail["width"];
    $height          =   $imageDetail["height"];

    if($width > $height) {
      $thumbWidth    =   $this->$thumbWidth;
      $thumbHeight    =   $old_y*($this->$thumbHeight/$old_x);
    }

    if($width < $height) {
      $thumbWidth    =   $old_x*($this->$thumbWidth/$old_y);
      $thumbHeight    =   $this->$thumbHeight;
    }

    if($width == $height) {
      $thumbWidth    =   $this->$thumbWidth;
      $thumbHeight    =   $this->$thumbHeight;
    }

    $thumbImage  =   ImageCreateTrueColor($thumbWidth,$thumbHeight);

    imagecopyresampled($thumbImage,$originalImage,0,0,0,0,$thumbWidth,$thumbHeight,$width,$height);


    // New save location
    $targetPath = $moveToDir . $image_name;

    $result = imagejpeg($thumb_image,$targetPath,80);

    imagedestroy($thumbImage);
    imagedestroy($originalImage);

    return $result;
  }
}
?>
