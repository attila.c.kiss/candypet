<?php
class Menu extends DB {
  function getAll() {
    return $this->select("SELECT * FROM `items`");
  }

  function getAllByUid($uid, $json = true) {
    $cat = $this->select("SELECT * FROM `categories` WHERE uid = " . $uid . " ORDER BY display_order");
    $response = [];
    foreach ($cat as $key=>$value) {
      $items = $this->select("SELECT * FROM `items` WHERE uid  = " . $uid . " AND cid = " . $value['id']);
      $response[$key]['cid'] = $value['id'];
      $response[$key]['cname'] = $value['name'];
      $response[$key]['order'] = $value['display_order'];
      if ($json) {
        $response[$key]['items'] = json_encode($items);
      } else {
        $response[$key]['items'] = $items;
      }
    }
    return $response;
  }

  function getItemsByCategory($uid, $cid) {
    return $this->select("SELECT * FROM `items` WHERE uid = " . $uid . " AND cid = " . $cid . " ORDER BY i_order");
  }

  function addItem($uid, $item) {
    $result = "";

    try {
      $next_order = 0;
      $spicy = 0;
      $veg = 0;
      $gf = 0;
      $s_price = null;

      if ($item->spicy == true) {
        $spicy = 1;
      }

      if ($item->veg == true) {
        $veg = 1;
      }

      if ($item->gf == true) {
        $gf = 1;
      }

      if($item->s_price != '') {
        $s_price = $item->s_price;
      }

      $last = $this->select("SELECT i_order FROM `items` WHERE uid = " . $uid . " ORDER BY i_order DESC LIMIT 1");
      if (sizeof($last) > 0) {
        $next_order = $last[0]['i_order'] + 1;
      } else {
        $next_order = 0;
      }

      $this->statement("INSERT INTO `items` (uid, cid, i_order, title, description, sm_price, lg_price, spicy, veg, gf)
      VALUES (:uid, :cid, :i_order, :title, :description, :sm_price, :lg_price, :spicy, :veg, :gf)",
      array(
        ':uid' => $uid,
        ':cid' => $item->cid,
        ':i_order' => $next_order,
        ':title' => $item->title,
        ':description' => $item->description,
        ':sm_price' => $s_price,
        ':lg_price' => $item->l_price,
        ':spicy' => $spicy,
        ':veg' => $veg,
        ':gf' => $gf
      ));
      $result = "OK";
    } catch (exception $e) {
      $result = "Error: " . $e;
    } finally {
      return $result;
    }
  }

  function update($uid, $item) {
    $result = "";
    $spicy = 0;
    $veg = 0;
    $gf = 0;
    $s_price = null;

    try {
      if ($item->spicy == true) {
        $spicy = 1;
      }

      if ($item->veg == true) {
        $veg = 1;
      }

      if ($item->gf == true) {
        $gf = 1;
      }

      if($item->s_price != '') {
        $s_price = $item->s_price;
      }

      $this->statement("UPDATE `items` SET cid = :cid, i_order = :i_order, title = :title, description = :description,
        sm_price = :sm_price, lg_price = :lg_price, spicy = :spicy, veg = :veg, gf = :gf WHERE uid = " . $uid . " AND id = " . $item->id,
        array(
          ':cid' => $item->cid,
          ':i_order' => $item->i_order,
          ':title' => $item->title,
          ':description' => $item->description,
          ':sm_price' => $s_price,
          ':lg_price' => $item->l_price,
          ':spicy' => $spicy,
          ':veg' => $veg,
          ':gf' => $gf
        )
      );
      $result = "OK";
    } catch (exception $e) {
      $result = "Error: " . $e;
    } finally {
      return $result;
    }
  }

  function move_up($uid, $id, $prev) {
    $result = "";
    try {
      $itm = $this->select("SELECT i_order FROM `items` WHERE uid = " . $uid . " AND id = " . $id);
      $prv = $this->select("SELECT i_order FROM `items` WHERE uid = " . $uid . " AND id = " . $prev);
      $this->statement("UPDATE `items` SET i_order = :i_order WHERE id = " . $id, array(':i_order'=>$prv[0]['i_order']));
      $this->statement("UPDATE `items` SET i_order = :i_order WHERE id = " . $prev, array(':i_order'=>$itm[0]['i_order']));
      $result = "OK";
    } catch (exception $e) {
      $result = "Error: " . $e;
    } finally {
      return $result;
    }
  }

  function move_down($uid, $id, $next) {
    $result = "";
    try {
      $itm = $this->select("SELECT i_order FROM `items` WHERE uid = " . $uid . " AND id = " . $id);
      $nxt = $this->select("SELECT i_order FROM `items` WHERE uid = " . $uid . " AND id = " . $next);
      $this->statement("UPDATE `items` SET i_order = :i_order WHERE id = " . $id, array(':i_order'=>$nxt[0]['i_order']));
      $this->statement("UPDATE `items` SET i_order = :i_order WHERE id = " . $next, array(':i_order'=>$itm[0]['i_order']));
      $result = "OK";
    } catch (exception $e) {
      $result = "Error: " . $e;
    } finally {
      return $result;
    }
  }

  function getById($id) {
    return $this->select("SELECT * FROM `items` WHERE id = " . $id);
  }

  function delete($uid, $id) {
    try {
      $this->statement("DELETE FROM `items` WHERE uid = :uid AND id = :id", array(":uid"=>$uid, ":id"=>$id));
      $result = "OK";
    } catch(exception $e) {
      $result = "Error: $e";
    } finally {
      return $result;
    }
  }
}
