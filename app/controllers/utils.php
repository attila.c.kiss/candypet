<?php
class Utils {
  function logout() {
    session_unset();
    session_destroy();
  }

  function keepalive() {
    $_SESSION['last_action'] = time();
  }

  function addJS($js) {
    if(isset($_SESSION['js_array'])) {
      array_push($_SESSION['js_array'], "<script src='" . $js . "'></script>\n" );
    } else {
      $_SESSION['js_array'] = array("<script src='" . $js . "'></script>\n");
    }
  }

  function getJS() {
    if(isset($_SESSION['js_array'])) {
      $js_print = "";
      foreach($_SESSION['js_array'] as $js) {
        $js_print .= $js;
      }
      unset($_SESSION['js_array']);

      return $js_print;
    }
  }

  function t($key) {
    return $key;
  }

  function addCSS($css) {
    $css_link = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $css . "\">\n";

    if(isset($_SESSION['css_array'])) {
      array_push($_SESSION['css_array'], $css_link );
    } else {
      $_SESSION['css_array'] = array($css_link);
    }
  }

  function getCSS() {
    if(isset($_SESSION['css_array'])) {
      $css_print = "";
      foreach($_SESSION['css_array'] as $css) {
        $css_print .= $css;
      }
      unset($_SESSION['css_array']);

      return $css_print;
    }
  }

  function array_search($array, $key, $value) {
    if(is_array($array)) {
      foreach($array as $item) {
        if($item[$key] == $value) {
          return $item;
        }
      }
    } else {
      return;
    }
  }
}
