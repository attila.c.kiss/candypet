<?php
class Category extends DB {
  function getAll($uid) {
    return $this->select("SELECT * FROM `categories` WHERE uid = " . $uid . " ORDER BY display_order ASC");
  }
  function getById($id) {
    return $this->select("SELECT * FROM `categories` WHERE id = " . $id);
  }

  function addCategory($uid, $name, $order) {
    return $this->statement("INSERT INTO `categories` (name, uid, display_order) VALUES (:name, :uid, :display_order)", array(':name' => $name, ':uid' => $uid, ':display_order' => $order));
  }

  function nextOrder($uid) {
    $last = $this->select("SELECT display_order FROM `categories` WHERE uid = " . $uid . " ORDER BY display_order DESC LIMIT 1");
    if (sizeof($last) > 0) {
      return $last[0]['display_order'] + 1;
    } else {
      return 0;
    }
  }

  function setOrder($id, $order) {
    return $this->statement("UPDATE `categories` SET display_order = :order WHERE id = $id", array(":order" => $order));
  }

  function delete($uid, $id) {
    try {
      $this->statement("DELETE FROM `categories` WHERE uid = :uid AND id = :id", array(":uid"=>$uid, ":id"=>$id));
      $this->statement("DELETE FROM `items` WHERE uid = :uid AND cid = :cid", array(":uid"=>$uid, ":cid"=>$id));
      $result = "OK";
    } catch(exception $e) {
      $result = "Error: $e";
    } finally {
      return $result;
    }
  }
}
