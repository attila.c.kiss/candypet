<?php
use Endroid\QrCode\QrCode;

class Users extends DB {
  function getAll() {
    return $this->select("SELECT * FROM `users`");
  }

  function getById($id) {
    return $this->select("SELECT * FROM `users` WHERE id = $id");
  }

  function getByEmail($email) {
    return $this->select("SELECT * FROM `users` WHERE email = $email");
  }

  function register($email, $password, $verification, $preflang, $currency) {
    $passwd = password_hash($password, PASSWORD_BCRYPT);
    return $this->statement("INSERT INTO `users` (email, password, verification, lang, currency) VALUES (:email, :passwd, :verification, :lang, :currency)", array(
      ':email' => $email,
      ':passwd' => $passwd,
      ':verification' => $verification,
      ':lang' => $preflang,
      ':currency' => $currency
    ));
  }

  function update($id, $lang, $currency, $rest_name, $rest_desc, $rest_footnote) {
    try {
      $slug = str_replace('?', '', $rest_name);
      $slug = str_replace('!', '', $slug);
      $slug = str_replace('#', '', $slug);
      $slug = str_replace('<', '', $slug);
      $slug = str_replace('>', '', $slug);
      $slug = str_replace('\'', '', $slug);
      $slug = str_replace('\"', '', $slug);
      $slug = str_replace('-', '_', $slug);
      $slug = str_replace('.', '', $slug);
      $slug = str_replace(',', '_', $slug);
      $slug = str_replace(' ', '_', $slug);
      $this->statement("UPDATE `users` SET lang = :lang, currency = :currency, rest_name = :rest_name, slug = :slug, rest_desc = :rest_desc, rest_footnote = :rest_footnote WHERE id = $id", array(
        ':lang' => $lang,
        ':currency' => $currency,
        ':rest_name' => $rest_name,
        ':slug' => $slug,
        ':rest_desc' => $rest_desc,
        ':rest_footnote' => $rest_footnote
      ));
      $filename = '/home/u689199123/domains/candypet.shop/public_html/restmen/public_html/images/qr_codes/'. $_SESSION['user_id'] . '.png';
      $qrCode = new QrCode($GLOBALS['settings']['basic']['site_domain'] . $id . '/' . $slug);
      $qrCode->setSize(200);
      if(file_exists($filename)) {
        unlink($filename);
      }
      $qrCode->writeFile($filename);

      $res = 'OK';
    } catch (exception $e) {
      $res = 'Error: ' . $e;
    } finally {
      return $res;
    }
  }

  function login($email, $password) {
    $usr = $this->select("SELECT * FROM `users` WHERE email LIKE '$email'");
    if(!empty($usr)) {
      $vrfy = password_verify($password, $usr[0]['password']);
      if($vrfy == true) {
        $_SESSION['user'] = $email;
        $_SESSION['user_id'] = $usr[0]['id'];
        $_SESSION['user_type'] = $usr[0]['user_type'];
        $_SESSION['lang'] = $usr[0]['lang'];
        $cur_res = $this->select("SELECT * FROM `currencies` WHERE id = " . $usr[0]['currency']);
        $_SESSION['currency'] = $cur_res[0];
        $this->statement("UPDATE `users` SET last_login = '" . date("Y-m-d H:i:s") . "' WHERE id = " . $usr[0]['id']);

        $result = "OK";
      } else {
        $result = "Username or password is incorrect.";
        unset($_SESSION['user']);
        session_destroy();
      }
    } else {
      $result = "Username or password is incorrect.";
      unset($_SESSION['user']);
      session_destroy();
    }
    return $result;
  }

  function checkemail($email) {
    return $this->select("SELECT 1 FROM `users` WHERE email LIKE '$email'");
  }

  function activate($verification) {
    $chkver = $this->select("SELECT 1 FROM `users` WHERE verification LIKE '$verification'");

    if(!empty($chkver)) {
      $this->statement("UPDATE `users` SET status = 'active', verification = '1'  WHERE verification LIKE '$verification'");
      $result = true;
    } else {
      $result = false;
    }

    return $result;
  }

  function restore($id) {
    $this->statement("UPDATE `users` SET status = 'active' WHERE id = $id");
  }

  function delete($id) {
    $this->statement("UPDATE `users` SET status = 'deleted' WHERE id = $id");
  }
}
