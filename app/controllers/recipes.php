<?php
class Recipes extends DB {
  function getAll($limit = 0) {
    if($limit > 0) {
      $lmt = "LIMIT " . $limit;
    } else {
      $lmt = "";
    }

    return $this->select("SELECT * FROM `recipes`" . $lmt);
  }

  function getById($id) {
    return $this->select("SELECT * FROM `recipes` WHERE id = '$id'")[0];
  }

  function new($title, $intro, $instructions, $notes, $preparing_time, $cooking_time, $status) {
    $this->statement("INSERT INTO `recipes` (title, intro, instructions, notes, preparing_time, cooking_time, status)
    VALUES (:title, :intro, :instructions, :notes, :preparing_time, :cooking_time, :status)",
    array(':title' => $title, ':intro' => strip_tags($intro), ':instructions' => strip_tags($instructions, "<ol>, <li>"), ':notes' => $notes, ':preparing_time' => $preparing_time, ':cooking_time' => $cooking_time, ':status' => $status ));
    return $this->select("SELECT LAST_INSERT_ID()");
  }

  function update($id, $title, $intro, $instructions, $notes, $preparing_time, $cooking_time, $status) {
    return $this->statement("UPDATE `recipes` SET title = :title, intro = :intro, instructions = :instructions, notes = :notes, preparing_time = :preparing_time, cooking_time = :cooking_time, status = :status WHERE id = :id",
    array(':title' => $title, ':intro' => strip_tags($intro), ':instructions' => strip_tags($instructions, "<ol>, <li>"), ':notes' => $notes, ':preparing_time' => $preparing_time, ':cooking_time' => $cooking_time, ':status' => $status, ':id' => $id));
  }

  function restore($id) {
    $this->statement("UPDATE `recipes` SET status = 'active' WHERE id = $id");
  }

  function delete($id) {
    $this->statement("UPDATE `recipes` SET status = 'deleted' WHERE id = $id");
  }

  function getTags() {
    return $this->select("SELECT * FROM `r_tags`");
  }

  function getTagById($id) {
    return $this->select("SELECT * FROM `r_tags` WHERE id = " . $id);
  }

  function getTagByItem($iid) {
    return $this->select("SELECT t.* FROM `r_tag_map` tm LEFT JOIN `r_tags` t ON t.id = tm.tid WHERE tm.iid = '$iid'");
  }

  function checkTag($iid, $tid) {
    var_dump($this->select("SELECT 1 FROM `r_tag_map` WHERE iid = '$iid' AND tid = '$tid' LIMIT 1"));
  }

  function addTag($iid, $tid) {
    if(empty($this->checkTag($iid, $tid))) {
      $this->statement("INSERT INTO `r_tag_map` (iid, tid) VALUES ('$iid', '$tid')");
    }
  }
}
